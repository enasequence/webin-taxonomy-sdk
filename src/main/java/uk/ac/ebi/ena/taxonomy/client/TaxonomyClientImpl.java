/*
 * Copyright 2018 EMBL - European Bioinformatics Institute
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package uk.ac.ebi.ena.taxonomy.client;

import org.apache.commons.lang.StringUtils;
import uk.ac.ebi.ena.taxonomy.taxon.SubmittableTaxon;
import uk.ac.ebi.ena.taxonomy.taxon.Taxon;
import uk.ac.ebi.ena.taxonomy.taxon.TaxonomyException;
import uk.ac.ebi.ena.taxonomy.util.TaxonUtils;
import uk.ac.ebi.ena.taxonomy.taxon.SubmittableTaxon.SubmittableTaxonStatus;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;


public class TaxonomyClientImpl implements TaxonomyClient
{
	private static final int DEFAULT_LIMIT = 10;

	private static TaxonomyClient taxonomyClient= new TaxonomyClientImpl();;
	private static Map<String, List <Taxon>> scientificNameTaxonCache = new HashMap<>();
	private static Map<String, List <Taxon>> commonNameTaxonCache = new HashMap<>();
	private static Map<Long, Taxon> taxIdTaxonCache = new HashMap<>();
	private static Map<String, SubmittableTaxon> submittableTaxonCache = new HashMap<>();
	private static Map<String, List <Taxon>> taxonAnyNameCache = new HashMap<>();

	@Override
	public List<Taxon> suggestTaxa(String partialName) throws TaxonomyException
	{
		return suggestTaxa(partialName, false, DEFAULT_LIMIT);
	}

	@Override
	public List<Taxon> suggestTaxa(String partialName, boolean metagenome) throws TaxonomyException
	{
		return suggestTaxa(partialName, metagenome, DEFAULT_LIMIT);
	}
	
	@Override
	public List<Taxon> suggestTaxa(String partialName, int limit) throws TaxonomyException
	{
		return suggestTaxa(partialName, false, limit);
	}

	@Override
	public List<Taxon> suggestTaxa(String partialName, boolean metagenome, int limit) throws TaxonomyException
	{
		if(partialName == null) return new ArrayList<>();
		String searchId = partialName + "?limit=" + limit;
		URL url;
		try {
			url = new URL(TaxonomyUrl.suggestForSubmission.get(searchId).replaceAll(" ", "%20"));
		} catch (MalformedURLException e) {
			throw new TaxonomyException(e);
		}
		if(!metagenome) {
			return TaxonUtils.getTaxons(url);
		} else {
			return TaxonUtils.getTaxons(url).stream().filter(this::isMetagenomic).collect(Collectors.toList());
		}
	}

	@Override
	public Taxon getTaxonByTaxid(Long taxId) throws TaxonomyException	{
		if (taxId == null)
			return null;

		Taxon taxon = taxIdTaxonCache.get(taxId);
		if (taxon == null) {
			try {
				URL url = new URL(TaxonomyUrl.taxid.get(taxId.toString()).replaceAll(" ", "%20"));
				List<Taxon> taxons = TaxonUtils.getTaxons(url);
				taxon =  !taxons.isEmpty() ? taxons.get(0) : null;
				if(taxon != null ){
					taxIdTaxonCache.put(taxId, taxon);
				}
			} catch (Exception e) {
				throw new TaxonomyException(e);
			}
		}
		return taxon;
	}

	@Override
	public List<Taxon> getTaxonByScientificName(String scientificName)throws TaxonomyException
	{
		if (scientificName == null) new ArrayList<>();
		String key = normalizeString(scientificName);
		List<Taxon> taxons= scientificNameTaxonCache.get(key);
		if(taxons == null) {
			try {
				URL url = new URL(TaxonomyUrl.scientificName.get(scientificName).replaceAll(" ", "%20"));
				taxons = TaxonUtils.getTaxons(url);
				
				if(taxons != null && !taxons.isEmpty()) {
					cacheTaxons(taxons, key, true);
				}
			} catch (Exception e) {
				throw new TaxonomyException(e);
			}
		}
		return taxons;
	}

	@Override
	public List<Taxon> getTaxonByCommonName(String commonName)throws TaxonomyException {
		if (commonName == null || commonName.isEmpty()) return new ArrayList<>();
		String key = normalizeString(commonName);
		List<Taxon> taxons = commonNameTaxonCache.get(key);
		if (taxons == null) {
			try {
				URL url = new URL(TaxonomyUrl.commonName.get(commonName).replaceAll(" ", "%20"));
				taxons = TaxonUtils.getTaxons(url);
				cacheTaxons(taxons, key, false);
			} catch (Exception e) {
				throw new TaxonomyException(e);
			}
		}
		return taxons;
	}

	@Override
	public List<Taxon> getTaxonByAnyName(String anyName)throws TaxonomyException
	{
		if (anyName == null||anyName.isEmpty())
			return new ArrayList<>();
		String key = normalizeString(anyName);
		List<Taxon> taxons = taxonAnyNameCache.get(key);
		if (taxons == null) {
			try {
				URL url = new URL(TaxonomyUrl.anyName.get(anyName).replaceAll(" ", "%20"));
				taxons = TaxonUtils.getTaxons(url);
				cacheTaxons(taxons, key, false);
			} catch (Exception e) {
				throw new TaxonomyException(e);
			}
		}
		return taxons;
	}

	@Override
	public SubmittableTaxon getSubmittableTaxonByScientificName(String scientificName)	throws TaxonomyException
	{
		return TaxonUtils.getSubmittableTaxon(getTaxonByScientificName(scientificName));

	}

	@Override
	public SubmittableTaxon getSubmittableTaxonByAnyName(String anyName)throws TaxonomyException
	{

		return TaxonUtils.getSubmittableTaxon(getTaxonByAnyName(anyName));

	}

	@Override
	public SubmittableTaxon getSubmittableTaxonByCommonName(String commonName)	throws TaxonomyException
	{
		return TaxonUtils.getSubmittableTaxon(getTaxonByCommonName(commonName));

	}

	@Override
	public SubmittableTaxon getSubmittableTaxonByTaxId(Long taxId)	throws TaxonomyException
	{
		return TaxonUtils.getSubmittableTaxon(getTaxonByTaxid(taxId) == null ? null: Collections.singletonList(getTaxonByTaxid(taxId)));
	}

	@Override
	public boolean isMetagenomic(Taxon taxon) throws TaxonomyException
	{
		if (taxon == null || taxon.getTaxId() == null)
			return false;
		if (taxon.getLineage() == null)
		{
			return getTaxonByTaxid(taxon.getTaxId()).isMetagenome();
		}
		return taxon.isMetagenome();

	}

	@Override
	public boolean isTaxIdValid(Long taxId) throws TaxonomyException
	{
			Taxon taxon = getTaxonByTaxid(taxId);
		return taxon != null;

	}

	@Override
	public boolean isScientificNameValid(String scientificName)
			throws TaxonomyException
	{
			List<Taxon> taxon = getTaxonByScientificName(scientificName);
		return !(taxon == null || taxon.isEmpty());
	}

	@Override
	public boolean isChildOfAny(String scientificName, String...parentScientificNames)
	{

		for(String parentName : parentScientificNames)
		{
			if(isChildOf(scientificName, parentName))
				return true;
		}
		return false;
	}

	@Override
	public boolean isNotChildOfAny(String scientificName, String...parentScientificNames)
	{
		return !isChildOfAny(scientificName,scientificName);
	}


	@Override
	public boolean isChildOf(String scientificName, String familyScientificName)
	{
		List <Taxon> taxons=getTaxonByScientificName(scientificName);
		if(taxons == null || taxons.isEmpty())
			return false;

		for(Taxon taxon:taxons) {
			if(taxon.isChildOf(familyScientificName))
				return true;
		}
		return false;
	}

	@Override
	public boolean isOrganismValid(String scientificName) {
		List<Taxon> taxons = getTaxonByScientificName(scientificName);
		return taxons != null && !taxons.isEmpty();
	}

	@Override
	public boolean isOrganismFormal(String scientificName) {
		List<Taxon> taxons = getTaxonByScientificName(scientificName);
		if(taxons != null && !taxons.isEmpty() ) {
			for (Taxon taxon : taxons) {
				if (taxon.isFormal())
					return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean isOrganismMetagenome(String scientificName)
	{
		List<Taxon> taxons = getTaxonByScientificName(scientificName);
		if(taxons == null || taxons.isEmpty())
			return false;
		for(Taxon taxon:taxons)
		{
			if(taxon.isMetagenome())
				return true;
		}
		return false;
	}

	@Override
	public boolean isProkaryotic(String scientificName) {
		if (scientificName != null
				&& isOrganismValid(scientificName)
				&& (isChildOf(scientificName, "Bacteria")
				|| isChildOf(scientificName, "Archaea"))) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isOrganismSubmittable(String scientificName) {
		return TaxonUtils.getSubmittableTaxonStatus( getTaxonByScientificName(scientificName) ) == SubmittableTaxonStatus.SUBMITTABLE_TAXON;
	}

	@Override
	public boolean isTaxidSubmittable(Long taxId) {
		Taxon taxon = getTaxonByTaxid(taxId);
		return taxon !=null && TaxonUtils.getSubmittableTaxonStatus(Collections.singletonList(taxon)) == SubmittableTaxonStatus.SUBMITTABLE_TAXON;
	}

	@Override
	public boolean isAnyNameSubmittable(String anyName) {
		String key = normalizeString(anyName);
		SubmittableTaxon taxon = submittableTaxonCache.get(key);
		if( taxon == null) {
			taxon = getSubmittableTaxonByAnyName(anyName);
			if(taxon == null) {
				return false;
			} else {
				submittableTaxonCache.put(key, taxon);
			}
		}
		return taxon.getSubmittableTaxonStatus() == SubmittableTaxonStatus.SUBMITTABLE_TAXON;
	}

	private void cacheTaxons(List<Taxon> taxons, String taxonName, boolean isScientific) {
		if( taxons !=null && !taxons.isEmpty()) {
			if(isScientific) {
				scientificNameTaxonCache.put(taxonName, taxons);
			} else {
				commonNameTaxonCache.put(taxonName, taxons);
			}
			taxons.forEach(taxon -> taxIdTaxonCache.put(taxon.getTaxId(), taxon) ) ;
		}
	}

	private String normalizeString(String string){
		if (string == null) {
			return  string;
		}
		return StringUtils.normalizeSpace(string.toUpperCase());
	}
}
