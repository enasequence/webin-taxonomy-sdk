/*
 * Copyright 2018 EMBL - European Bioinformatics Institute
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package uk.ac.ebi.ena.taxonomy.client;

import org.apache.commons.lang.StringUtils;
import uk.ac.ebi.ena.taxonomy.taxon.SubmittableTaxon;
import uk.ac.ebi.ena.taxonomy.taxon.Taxon;
import uk.ac.ebi.ena.taxonomy.taxon.TaxonomyException;
import uk.ac.ebi.ena.taxonomy.util.TaxonUtils;
import uk.ac.ebi.ena.taxonomy.taxon.SubmittableTaxon.SubmittableTaxonStatus;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaxonomyClientCacheImpl implements TaxonomyClient
{
	private TaxonomyClient taxonomyClient ;
	private static Map<String, List<Taxon>> taxonScientificNameCache = Collections.synchronizedMap(new HashMap<String, List<Taxon>>());
	private static Map<Long, Taxon> taxonIdCache = Collections.synchronizedMap(new HashMap<Long, Taxon>());
	private static Map<String, List<Taxon>> taxonCommonNameCache = Collections.synchronizedMap(new HashMap<String, List<Taxon>>());
	private static Map<String, List<Taxon>> taxonAnyNameCache = Collections.synchronizedMap(new HashMap<String, List<Taxon>>());
	private static Map<String, SubmittableTaxon> submittableTaxonCache = new HashMap<>();
	private static Map<String, List <Taxon>> scientificNameTaxonCache = new HashMap<>();
	private static Map<String, List <Taxon>> commonNameTaxonCache = new HashMap<>();
	private static Map<Long, Taxon> taxIdTaxonCache = new HashMap<>();
	
	private static final int DEFAULT_LIMIT = 10;

	public TaxonomyClientCacheImpl(TaxonomyClient taxonomyClient) {
		this.taxonomyClient = taxonomyClient;
	}
	public TaxonomyClientCacheImpl() {
		this.taxonomyClient = new TaxonomyClientCacheImpl(new TaxonomyClientImpl());
	}

	@Override
	public List<Taxon> suggestTaxa(String partialName) throws TaxonomyException
	{
		return taxonomyClient.suggestTaxa(partialName, false, DEFAULT_LIMIT);
	}

	@Override
	public List<Taxon> suggestTaxa(String partialName, boolean metagenome) throws TaxonomyException
	{
		return taxonomyClient.suggestTaxa(partialName, metagenome, DEFAULT_LIMIT);
	}

	@Override
	public List<Taxon> suggestTaxa(String partialName, int limit) throws TaxonomyException
	{
		return taxonomyClient.suggestTaxa(partialName, false, limit);
	}


	public List<Taxon> suggestTaxa(String partialName, boolean metagenome, int limit) throws TaxonomyException
	{
		return taxonomyClient.suggestTaxa(partialName, metagenome, limit);
	}

	@Override
	public Taxon getTaxonByTaxid(Long taxId) throws TaxonomyException
	{
		return taxonIdCache.computeIfAbsent(taxId, taxonomyClient::getTaxonByTaxid);
	}

	@Override
	public List<Taxon> getTaxonByScientificName(String scientificName)throws TaxonomyException
	{
		return taxonScientificNameCache.computeIfAbsent(scientificName, taxonomyClient::getTaxonByScientificName);
	}

	@Override
	public List<Taxon> getTaxonByCommonName(String commonName)throws TaxonomyException
	{
		return taxonCommonNameCache.computeIfAbsent(commonName, taxonomyClient::getTaxonByCommonName);
	}

	@Override
	public List<Taxon> getTaxonByAnyName(String anyName)throws TaxonomyException
	{
		return taxonAnyNameCache.computeIfAbsent(anyName, taxonomyClient::getTaxonByAnyName);
	}

	@Override
	public SubmittableTaxon getSubmittableTaxonByScientificName(String scientificName)	throws TaxonomyException
	{
		return taxonomyClient.getSubmittableTaxonByScientificName(scientificName);

	}

	@Override
	public SubmittableTaxon getSubmittableTaxonByAnyName(String anyName)throws TaxonomyException
	{
		return taxonomyClient.getSubmittableTaxonByAnyName(anyName);

	}

	@Override
	public SubmittableTaxon getSubmittableTaxonByCommonName(String commonName)	throws TaxonomyException
	{
		return taxonomyClient.getSubmittableTaxonByAnyName(commonName);

	}

	@Override
	public SubmittableTaxon getSubmittableTaxonByTaxId(Long taxId)	throws TaxonomyException
	{
		return taxonomyClient.getSubmittableTaxonByTaxId(taxId);
	}

	@Override
	public boolean isMetagenomic(Taxon taxon) throws TaxonomyException
	{
		if (taxon == null || taxon.getTaxId() == null)
			return false;
		if (taxon.getLineage() == null)
		{
			return getTaxonByTaxid(taxon.getTaxId()).isMetagenome();
		}
		return taxon.isMetagenome();

	}

	@Override
	public boolean isTaxIdValid(Long taxId) throws TaxonomyException
	{
		Taxon taxon = getTaxonByTaxid(taxId);
		return taxon != null;
	}

	@Override
	public boolean isScientificNameValid(String scientificName)
			throws TaxonomyException
	{

		List<Taxon> taxon = getTaxonByScientificName(scientificName);
		return !(taxon == null || taxon.isEmpty());

	}

	@Override
	public boolean isChildOfAny(String scientificName, String...parentScientificNames)
	{

		for(String parentName : parentScientificNames)
		{
			if(isChildOf(scientificName, parentName))
				return true;
		}
		return false;
	}

	@Override
	public boolean isNotChildOfAny(String scientificName, String...parentScientificNames)
	{
		return !isChildOfAny(scientificName,scientificName);
	}


	@Override
	public boolean isChildOf(String scientificName, String familyScientificName)
	{
		List <Taxon> taxons=getTaxonByScientificName(scientificName);
		if(taxons == null || taxons.isEmpty())
			return false;

		for(Taxon taxon:taxons) {
			if(taxon.isChildOf(familyScientificName))
				return true;
		}
		return false;
	}

	@Override
	public boolean isOrganismValid(String scientificName) {
		List<Taxon> taxons = getByScientificName(scientificName);
		return taxons != null && !taxons.isEmpty();
	}

	@Override
	public boolean isOrganismFormal(String scientificName) {
		List<Taxon> taxons = getByScientificName(scientificName);
		if(taxons != null && !taxons.isEmpty() ) {
			for (Taxon taxon : taxons) {
				if (taxon.isFormal())
					return true;
			}
		}
		return false;
	}



	@Override
	public boolean isOrganismMetagenome(String scientificName)
	{
		List<Taxon> taxons = getByScientificName(scientificName);
		if(taxons == null || taxons.isEmpty())
			return false;
		for(Taxon taxon:taxons)
		{
			if(taxon.isMetagenome())
				return true;
		}
		return false;
	}

	@Override
	public boolean isProkaryotic(String scientificName) {
		if (scientificName != null
				&& isOrganismValid(scientificName)
				&& (isChildOf(scientificName, "Bacteria")
				|| isChildOf(scientificName, "Archaea"))) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isOrganismSubmittable(String scientificName) {
		return TaxonUtils.getSubmittableTaxonStatus( getTaxonByScientificName(scientificName) ) == SubmittableTaxonStatus.SUBMITTABLE_TAXON;
	}

	@Override
	public boolean isTaxidSubmittable(Long taxId) {
		Taxon taxon = getTaxonByTaxid(taxId);
		return taxon !=null && TaxonUtils.getSubmittableTaxonStatus(Collections.singletonList(taxon)) == SubmittableTaxonStatus.SUBMITTABLE_TAXON;
	}

	@Override
	public boolean isAnyNameSubmittable(String anyName) {
		String key = normalizeString(anyName);
		SubmittableTaxon taxon = submittableTaxonCache.get(key);
		if( taxon == null) {
			taxon = taxonomyClient.getSubmittableTaxonByAnyName(anyName);
			if(taxon == null) {
				return false;
			} else {
				submittableTaxonCache.put(key, taxon);
			}
		}
		return taxon.getSubmittableTaxonStatus() == SubmittableTaxonStatus.SUBMITTABLE_TAXON;
	}


	private List<Taxon> getByScientificName(String scientificName) {
		String key = normalizeString(scientificName);
		List<Taxon> taxons= scientificNameTaxonCache.get(key);
		if(taxons == null) {
			try {
				taxons = taxonomyClient.getTaxonByScientificName(scientificName);
				cacheTaxons(taxons, key, true);
			} catch (Exception e) {
				return null;
			}
		}
		return taxons;
	}

	private void cacheTaxons(List<Taxon> taxons, String taxonName, boolean isScientific) {
		if( taxons !=null && !taxons.isEmpty()) {
			if(isScientific) {
				scientificNameTaxonCache.put(taxonName, taxons);
			} else {
				commonNameTaxonCache.put(taxonName, taxons);
			}
			taxons.forEach(taxon -> taxIdTaxonCache.put(taxon.getTaxId(), taxon) ) ;
		}
	}

	private String normalizeString(String string){
		if (string == null) {
			return  string;
		}
		return StringUtils.normalizeSpace(string.toUpperCase());
	}

}
